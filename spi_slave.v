`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.04.2019 15:09:37
// Design Name: 
// Module Name: spi_slave
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// clk - 50Mhz
//
//
module spi_slave(
    input               CLK,
    input               RST1,
    input wire          SCL,
    input wire          MOSI,
    output reg          MISO = 1'hz,
    input wire          CS,
    output reg [7:0]    OU_TEST,
    input wire  [7:0]    IN_TEST,// 
    output wire [1:0]   MODE_1,
    output wire [1:0]   MODE_2,
    output wire         interupt,
	 output wire 			LED
    );
    
    reg [3:0] assessment = 4'd0;
	 reg [3:0] assessment_CS = 4'd0;
    reg [7:0] reg_MOSI = 8'd0;
    reg [7:0] reg_MISO = 8'd140;
    reg [3:0] cnt = 4'd0;
	 reg [1:0] cnt_word = 2'd0;
	 reg [7:0] TRST_REG = 8'd10;
    
    wire finish;
    wire RST;
	 assign RST = ~RST1;
    
    wire [7:0] transmit_data;
    
   // wire [1:0] m1;

        
    reg ready = 1'b0;
/////////////////////////////////////////////////////////////////////////////////////    

/////////////////////////////////////////////////////////////////////////////////////    
// interface spi 
 always@(posedge CLK)
    if(RST)
    begin
     assessment <= 4'd0;
	  assessment_CS <= 4'd0;
    end 
    else begin
    if(SCL)
         assessment <= {assessment[2:0],1'd1};// оценка 
    else
         assessment <= {assessment[2:0],1'd0};
	 assessment_CS<={assessment_CS[2:0], CS};
    end 
///////////////////////////////////////////////// 
 always@(posedge CLK)
    if(RST) begin 
        reg_MOSI 	<= 8'd0;
        reg_MISO 	<= 8'd140;
        ready 		<= 1'b0;
		  OU_TEST <= 8'd255;
		  //TRST_REG 	<= 8'd10;
       end
      else if(!CS) begin 
            if(assessment == 4'b0011)begin 
                reg_MOSI <= {reg_MOSI[6:0], MOSI};
            end 
            if(assessment == 4'b1100 | assessment_CS == 4'b1100)begin 
               MISO <= reg_MISO[7];
               reg_MISO <= {reg_MISO[6:0], 1'd0};
            end 
            if((assessment == 4'b0011)& cnt < 4'd8)begin 
                cnt <= cnt + 1'd1; 
            end  
            else begin 
             if(cnt >= 4'd8) begin 
                cnt <=  4'd0;
					 cnt_word <= cnt_word + 1'd1;
					 
					 if(cnt_word == 2'd0) begin 
							reg_MISO <= cnt_word;
							OU_TEST <= reg_MOSI;

							end 
						else if(cnt_word == 2'd1) begin 
									reg_MISO <= cnt_word;//;transmit_data
									OU_TEST <= reg_MOSI;
									end 
							else if(cnt_word == 2'd2) begin
									TRST_REG <= IN_TEST;
							
									
							end 
                ready <= 1'b1;
            end
            else ready <= 1'b0;
      end 
      end 
           else begin 
                 reg_MISO <= TRST_REG;//; transmit_data
                 MISO <= 1'hZ;
                 cnt <=  4'd0;
                 reg_MOSI <= 8'd0;
					  OU_TEST <= 8'd0;
                 end 
	  ///////////////////////////////////////////////
	 
	 reg [23:0] counter;

always@(posedge CLK)
	if(RST)
		counter <= 24'd0;
	else
	begin 
		counter <= counter + 1'd1;
		//OU_TEST <= counter[]
	end 
	
	assign LED = counter[23];
	 
 /////////////////////////////////////////////////////////////////////////////////////    

  // blocs of analisis 
  // reg_MOSI <- transmit & ready 
  // ready <- finish recive data and transmit data 
  // transmit_data - write then cnt == 7
  /*
  controller control1(
        .CLK        (CLK),
        .RST        (RST),
        .operand    (reg_MOSI),
        .ready      (ready),
        .start      (!CS),
        .finish     (finish),
        .result     (transmit_data),
        .ansver_b   (IN_TEST), //IN_TEST 
        .quest      (OU_TEST),//OU_TEST
        .interupt   (interupt)//,
      //  .m1(m1),
      //  .m2(m2)             
      );
      */
 /////////////////////////////////// 
 //  first bufer is on as input 
 //  second buffer is on as output   
 assign MODE_1 = 2'b00;//m1;//
 assign MODE_2 = 2'b01;//m2;//
 //assign OU_TEST = {counter[23:20], counter[23:20]};
 //assign IN_TEST = counter[23:16];
endmodule
