`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.04.2019 17:16:35
// Design Name: 
// Module Name: controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module controller(
    input CLK,
    input RST,
    input [7:0] operand,
    input  ready,
    input       start,
    output reg     finish,
    output reg [7:0] result,
    
    input wire [7:0] ansver_b, 
    output wire [7:0] quest,   
    output reg interupt = 1'h0//,
   // output reg [1:0] m1 = 2'd2,
   // output reg [1:0] m2 = 2'd2    
    );
    // // // // // // // // // // // // // // //
    reg [7:0] command = 8'd0;
    reg [7:0] mask = 8'd0;
    reg [7:0] result_cmp = 8'd0;
    // // // // // // // // // // // // // // //
    reg [7:0] delay = 8'd0;
    wire [7:0] sig_dev;
    reg  dev = 1'd0;
    wire treatment_sig;
    reg [1:0]count = 2'd0;
    // // // // // // // // // // // // // // //
    reg [7:0] count_one = 8'd0;
    reg [7:0] count_zerow = 8'd0;
    
  ///////////////////////////////////////////////   
    always@(posedge CLK)
        if(RST)
            count = 2'd0;
           else if(ready) begin 
                    
                    if(count >= 2'd2)
                        count <= 2'd0;
                    else 
                        count <= count +1'd1;
                    end 
                 else 
                    if(!start)
                        count <= 2'd0;
 ///////////////////////////////////////////////
    always@(posedge CLK)
        if(RST)
            begin 
                command <= 8'd0;
                mask <= 8'd0;
                result_cmp <= 8'd0;
                delay <= 8'd0;
                result <= 8'd0;
                finish <= 1'd0;
                count_one <= 8'd0;
                count_zerow <= 8'd0;
                // m1 <= 2'd2;
                // m2 <= 2'd2;
            end 
         else begin 
                if(start) begin
                  //  m1 <= 2'd2;
                  //  m2 <= 2'd2; 
                    interupt <= 1'd0;
                    finish <= 1'd0;
                    delay <= 8'd0;
                    //$display("count =  %d",count);
                    if(count == 2'd0 & ready) begin 
                        command <= operand;
                       // $display("command = %d <<<<<<<<<",operand);
                    end 
                    else if(count == 2'd1 & ready) begin
                         mask <= operand;
                        // $display("mask = %d",operand);
                    end
                     else if(count == 2'd2 & ready) begin
                            result_cmp <= operand;
                           // $display("result_cmp = %d ********<<<<<",operand);
                     end 
                end
                else begin
                    //$display("start = 0"); 
                    if(delay >= 8'd250 & command == 8'd10) begin  
                       // $display("delay = %d",delay);
                      // m1 <= 2'd2;
                      // m2 <= 2'd2;
                       command <= 8'd0;
                        mask <=  8'd0;
                        result_cmp <= 8'd0;
                        if(ansver_b == result_cmp) begin 
                            result <= 8'hFF;
                            interupt <= 1'd1;
                            end 
                          else begin 
                            result <= ansver_b;
                            interupt <= 1'd1;
                            end 
                         finish <= 1'd1;
                          end 
                    else 
                    if(command == 8'd10) begin
                         delay <= delay + 1'd1;
                         //m1 <= 2'd0;
                        // m2 <= 2'd1;
                          //$display(" m1 m2 1 command == 8'd10");
                         end 
                    else 
                        /*if(command == 8'd30 & delay < 8'd250) begin //// косяк
                           // m1 <= 2'd0;
                           // m2 <= 2'd1;
                           // $display(" m1 m2 2 command == 8'd30");
                            if(treatment_sig)begin
                                count_one <= count_one +1'd1; 
                            end 
                            else 
                                count_zerow <= count_zerow +1'd1;
                                delay <= delay +1'd1;
                         end 
                           else*/ if( delay == 8'd250) begin
                             delay <= delay +1'd1;
                              result <= count_one - count_zerow; 
                           end else 
                            if( delay == 8'd251) begin
                                finish <= 1'd1;
                                //m1 <= 2'd3;
                                //m2 <= 2'd2;
                                 command <= 8'd0;
                                 mask <=  8'd0;
                                 result_cmp <= 8'd0;
                                count_one <= 8'd0;
                                count_zerow <= 8'd0;
                                delay <= delay +1'd1;
                                interupt <= 1'd1;
                              end                           
                end 
         end 
    ///////////////////////////////////////////////////////////       
         always@(posedge CLK) // clk 25MHz
             if(RST) begin 
                dev <= 1'd0;
             end
             else 
                begin 
                    dev <=  dev +1'd1; 
                end 
    
	 //////////////////////////////////////////////
    assign sig_dev = (dev)? 8'hFF : 8'h00;
    assign quest = (command == 8'd10)? mask :((command == 8'd30)? mask & sig_dev : 8'h00);
    assign treatment_sig =  &(~mask | ansver_b) ;
endmodule
