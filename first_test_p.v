module first_test_p(
input CLK,
output wire LED
);

reg [23:0] counter;

always@(posedge CLK)
	begin 
		counter = counter + 1'd1;
	end 
	
	assign LED = counter[23];

endmodule
